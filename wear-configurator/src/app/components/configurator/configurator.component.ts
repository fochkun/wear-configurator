import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ModelInfoService } from 'src/app/services/model-info.service';

@Component({
  selector: 'app-configurator',
  templateUrl: './configurator.component.html',
  styleUrls: ['./configurator.component.less']
})
export class ConfiguratorComponent implements OnInit {

  mainColors: string[] = [
    '#090b0a',
    '#f3403c',
    '#787d83',
    '#e6ebee',
    '#0061b4'
  ];
  addColors: string[] = [
    '#090b0a',
    '#f3403c',
    '#787d83',
    '#e6ebee',
    '#0061b4'
  ];
  lineColors: string[] = [
    '#090b0a',
    '#f3403c',
    '#787d83',
    '#e6ebee',
    '#0061b4'
  ];

  mainSelect = 0;
  addSelect = 0;
  lineSelect = 0;
  constructor(private modelInfo: ModelInfoService) { }

  ngOnInit() {
  }

  changeColor(id, color, idx) {
    if (id === "main") {
      this.mainSelect = idx;
      this.modelInfo.mainColor = color;
    } else if (id === 'add') {
      this.addSelect = idx;
      this.modelInfo.addColor = color;
    } else if (id === 'line') {
      this.lineSelect = idx;
      this.modelInfo.lineColor = color;
    }

  }

}
