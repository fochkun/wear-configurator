import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ModelInfoService } from 'src/app/services/model-info.service';

@Component({
  selector: 'app-model-viewer',
  templateUrl: './model-viewer.component.html',
  styleUrls: ['./model-viewer.component.less']
})
export class ModelViewerComponent implements OnInit, AfterViewInit {

  private donors: {} = {};
  constructor(private modelInfo: ModelInfoService) { }

  ngOnInit() {
    this.modelInfo.callbackMainColorChange = () => {
      const id = 'img-main';
      const donor = this.donors[id] ? this.clone(this.donors[id]) : undefined;
      console.log('have donor', donor, this.donors);
      this.tintMyImg(id, this.modelInfo.mainColor, true, donor);
    };
    this.modelInfo.callbackAddColorChange = () => {
      const id = 'img-add-1';
      const donor = this.donors[id] ? this.clone(this.donors[id]) : undefined;
      this.tintMyImg(id, this.modelInfo.addColor, true, donor);
    };
    this.modelInfo.callbackLineColorChange = () => {
      const id = 'img-add-2';
      const donor = this.donors[id] ? this.clone(this.donors[id]) : undefined;
      this.tintMyImg(id, this.modelInfo.lineColor, true, (donor as ImageData));
    };
  }

  ngAfterViewInit() {


  }

  onImageLoaded(id: string) {
    if (!this.tinted.includes(id)) {
      this.tinted.push(id);
      const image: any = document.getElementById(id);
      console.log('image loaded', image);
      const canvas = document.createElement("canvas");
      canvas.width = image.offsetWidth;
      canvas.height = image.offsetHeight;
      const ctx = canvas.getContext("2d");
      ctx.drawImage(image, 0, 0);
      let imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
      console.log('image data is', imageData);
      this.donors[id] = this.clone(imageData);
    }

  }
  private tinted: string[] = [];
  tintMyImg(id: string, color: string, force = false, donor?) {
    if (force) {
      console.log('force power', donor);
    }
    if (force) {
      console.log('change color', color.toLowerCase(), donor);
      this.tinted.push(id);
      const elem = document.getElementById(id);
      this.tintImage(document.getElementById(id), color.toLowerCase(), donor);

    }
  }

  tintImage(imgElement, tintColor, donor?) {
    // create hidden canvas (using image dimensions)
    const canvas = document.createElement("canvas");
    const overlayCanvas = document.createElement("canvas");
    const RGBColor = this.hexToRgb(tintColor);
    canvas.width = imgElement.offsetWidth;
    canvas.height = imgElement.offsetHeight;

    const ctx = canvas.getContext("2d");
    const ctx1 = overlayCanvas.getContext("2d");
    ctx.drawImage(imgElement, 0, 0);
    ctx1.drawImage(imgElement, 0, 0);
    console.log('donor is ', donor);
    if (donor) {
      console.log('use donor', donor);
    } else {
      console.log('have no donor', donor);
    }
    let map = ctx.getImageData(0, 0, canvas.width, canvas.height);
    //map.data = donor.data;
    // for (let idx = 0; idx < map.data.length; idx++) {
    //   map.data[idx] = donor[idx] ? donor[idx] : map.data[idx];
    // }
    console.log('map', map);
    const imdata = map.data;
    let map1 = ctx.getImageData(0, 0, canvas.width, canvas.height);
    const imdata1 = map1.data;
    for (let idx = 0; idx < map.data.length; idx++) {
      imdata[idx] = imdata1[idx] = donor.data[idx];
    }

    // convert image to grayscale
    let r;
    let g;
    let b;
    let avg;
    for (let point = 0, len = imdata.length; point < len; point += 4) {
      r = imdata[point];
      g = imdata[point + 1];
      b = imdata[point + 2];
      // const min=Math.min(r,g,b);

      avg = Math.floor((r + g + b) / 3);
      const param = 0;
      const limit = 255;
      imdata1[point] = avg >= param ? RGBColor.r : 0;
      imdata1[point + 1] = avg >= param ? RGBColor.g : 0;
      imdata1[point + 2] = avg >= param ? RGBColor.b : 0;
      imdata[point] = imdata[point + 1] = imdata[point + 2] = avg;
      imdata[point] = imdata[point] + imdata1[point] < limit ? imdata[point] + imdata1[point] : limit;
      imdata[point + 1] = imdata[point + 1] + imdata1[point + 1] < limit ? imdata[point + 1] + imdata1[point + 1] : limit;
      imdata[point + 2] = imdata[point + 2] + imdata1[point + 2] < limit ? imdata[point + 2] + imdata1[point + 2] : limit;
      imdata[point + 3] = imdata[point + 3] + imdata1[point + 3] < limit ? imdata[point + 3] + imdata1[point + 3] : limit;
    }

    ctx.putImageData(map, 0, 0);

    ctx.globalCompositeOperation = "lighter";
    ctx.globalAlpha = 1;
    imgElement.src = canvas.toDataURL();
  }

  hexToRgb(hex) {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null;
  }

  clone(imageData) {
    let canvas = document.createElement('canvas');
    canvas.width = imageData.width;
    canvas.height = imageData.height;
    let context = canvas.getContext('2d');
    context.putImageData(imageData, 0, 0);
    return context.getImageData(0, 0, imageData.width, imageData.height);
  };

}
