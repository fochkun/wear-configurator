import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ModelInfoComponent } from './components/model-info/model-info.component';
import { EditPageComponent } from './pages/edit-page/edit-page.component';
import { ConfiguratorComponent } from './components/configurator/configurator.component';
import { ModelViewerComponent } from './components/model-viewer/model-viewer.component';

@NgModule({
  declarations: [
    AppComponent,
    ModelInfoComponent,
    EditPageComponent,
    ConfiguratorComponent,
    ModelViewerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
