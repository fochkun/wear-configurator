import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModelInfoService {

  private _mainColor: string = "#ffaabb";
  private _addColor: string = "#ffaabb";
  private _lineColor: string = "#ffaabb";
  public callbackMainColorChange: () => void;
  public callbackAddColorChange: () => void;
  public callbackLineColorChange: () => void;
  constructor() { }

  set mainColor(val: string) {
    this._mainColor = val;
    console.log('main color is ', val);
    if (this.callbackMainColorChange) {
      this.callbackMainColorChange();
    }
  }
  get mainColor(): string {
    return this._mainColor;
  }

  set addColor(val: string) {
    this._addColor = val;
    if (this.callbackAddColorChange) {
      this.callbackAddColorChange();
    }
  }
  get addColor(): string {
    return this._addColor;
  }

  set lineColor(val: string) {
    this._lineColor = val;
    if (this.callbackLineColorChange) {
      this.callbackLineColorChange();
    }
  }
  get lineColor(): string {
    return this._lineColor;
  }

}
